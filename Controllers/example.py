import requests
import re
from bs4 import BeautifulSoup


website = "https://www.imdb.com/list/ls569826228/?sort=user_rating,desc&st_dt=&mode=detail&page=1"
resultado = requests.get(website)
soup = BeautifulSoup(resultado.content, "html.parser")

titulos = soup.find_all("a", href=re.compile(r"/title/tt[\w-]*"))

for titulo in titulos:
    print(titulo.text.strip())
