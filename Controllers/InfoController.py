import re
from http.client import HTTPException
from fastapi import APIRouter
import requests
router = APIRouter()

@router.get("/GetbestFilms")
def Get_bestfilms():
    website = "https://www.imdb.com/list/ls569826228/?sort=user_rating,desc&st_dt=&mode=detail&page=1"
    resultado = requests.get(website)
    patron=r"/title/[\w-]*"
    content = resultado.text
    maquinas_repetidas=re.findall(patron,content)

    if resultado.status_code == 200:
        return {"message": "Solicitud exitosa", "content": maquinas_repetidas}
    else:
        raise HTTPException(status_code=resultado.status_code, detail="Error al realizar la solicitud")

@router.post("/GetWorstFilms")
def GetWorstFilms():
    return {"message": "Crear papas"}

# Otros endpoints de papas...
