from fastapi import FastAPI
from Controllers.InfoController import router as Infocontroller
app=FastAPI()
#agregar coleciones en fastapi
app.include_router(Infocontroller,tags=["Info"])


@app.get("/")

def index():
    return {"message":"hola,pythoneanos"}

@app.get("/libros/{id}")
def mostar_libro(id: int):
    return{"data":id}


@app.post("/libros")
def insertar_libro():
    return{"message":"libro insertado papi"}